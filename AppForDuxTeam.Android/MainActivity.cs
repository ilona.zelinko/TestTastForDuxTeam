﻿using Android.App;
using Android.OS;
using Android.Widget;
using System;

namespace AppForDuxTeam.Android
{
    [Activity(Label = "AppForDuxTeam.Android", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);


            Button btnClick = FindViewById<Button>(Resource.Id.btnClick);
            TextView tvInfoBox = FindViewById<TextView>(Resource.Id.tvInfoBox);

            btnClick.Click += (object sender, EventArgs e) =>
            {
                string date = DateTime.Now.ToShortDateString();
                string time = DateTime.Now.ToLongTimeString(); ;
                tvInfoBox.Text = String.Format("Date: {0}\nTime: {1}", date, time);
            };
        }
    }
}

