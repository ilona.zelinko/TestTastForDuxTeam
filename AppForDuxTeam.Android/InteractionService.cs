﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace AppForDuxTeam.Android
{
    class InteractionService
    {
        const string Url = "http://192.168.0.103:3189/clickhistory/GetAllRecords/";
        private HttpClient GetClient()
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Accept", "application/json");
            return client;
        }

        public async Task<IEnumerable<ClickInfo>> Get()
        {
            HttpClient client = GetClient();
            string result = await client.GetStringAsync(Url);
            return JsonConvert.DeserializeObject<IEnumerable<ClickInfo>>(result);
        }

        public async Task<ClickInfo> Add(ClickInfo info)
        {
            HttpClient client = GetClient();
            var response = await client.PostAsync(Url,
                new StringContent(
                    JsonConvert.SerializeObject(info),
                    System.Text.Encoding.UTF8, "application/json"));

            if (response.StatusCode != HttpStatusCode.OK)
                return null;

            return JsonConvert.DeserializeObject<ClickInfo>(
                await response.Content.ReadAsStringAsync());
        }
    }
}