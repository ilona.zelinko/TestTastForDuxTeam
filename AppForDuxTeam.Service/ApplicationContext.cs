﻿using AppForDuxTeam.Service.Models;
using System.Data.Entity;

namespace AppForDuxTeam.Service
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext() : base("DefaultConnection")
        {
        }

        public DbSet<ButtonClicksHistory> ButtonClicks { get; set; }
    }
}