﻿using AppForDuxTeam.Service.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace AppForDuxTeam.Service.Controllers
{
    public class ClicksHistoryController : ApiController
    {
        ApplicationContext db = new ApplicationContext();

        public IEnumerable<ButtonClicksHistory> GetAllRecords()
        {
            return db.ButtonClicks.ToList();
        }

        public IHttpActionResult AddRecord(ButtonClicksHistory item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.ButtonClicks.Add(item);
            db.SaveChanges();
            return Ok(item);
        }
    }
}
