﻿namespace AppForDuxTeam.Service.Models
{
    public class ButtonClicksHistory
    {
        public int Id { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
    }
}